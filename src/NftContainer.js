import React from "react";
import NFTCard from "./NftCard";
const NFTContainer = ({listCollected}) => {
    return (
        <div>
            {listCollected.map((nft, index) => {
                return <NFTCard nft={nft} key={index}/>
            })}
        </div>
    )
}

export default NFTContainer;
