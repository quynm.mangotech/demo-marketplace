import React, { useEffect, useState } from 'react';
import {ethers} from 'ethers';
import axios from 'axios';
import './App.css';
import NFTContainer from './NftContainer';

function App() {
  const [defaultAccount, setDefaultAccount] = useState('');
  const [userBalance, setUserBalance] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [buttonText, setButtonText ] = useState("Connect Wallet");
  const [listCollected, setListCollected] = useState([]);
  useEffect( () => {
    async function fetchData(){
      if(defaultAccount) {
        setListCollected(await getListNft(defaultAccount));
      }
    }
    fetchData();
  }, [defaultAccount])

  if(window.ethereum) {
    window.ethereum.on('accountsChanged',  accountChangedHandler);
    window.ethereum.on('chainChanged', chainChangedHandler);
  }

  function chainChangedHandler() {
    window.location.reload();
  }

  async function getUserBalance(account) {
    if(account) {
      const balance = await window.ethereum.request({ method: 'eth_getBalance', params: [account, 'latest']});
      setUserBalance(ethers.utils.formatEther(balance))
    } else {
      setUserBalance('')
      setButtonText("Connect Wallet")
    }
  }

  async function requestAccount() {
    if (window.ethereum) {
      const accounts = await window.ethereum.request({ method: 'eth_requestAccounts'});
      await accountChangedHandler(accounts);
      setButtonText("Wallet connected");
    } else {
      setErrorMessage("Need to install Metamask");
    }
  }

  async function accountChangedHandler(newAccount) {
    setDefaultAccount(newAccount[0]);
    await getUserBalance(newAccount.toString())
  }

  async function getListNft(addressAccount) {
    const options = {
      method: 'GET',
      url: `https://deep-index.moralis.io/api/v2/${addressAccount}/nft`,
      params: {chain: 'goerli', format: 'decimal', normalizeMetadata: 'false'},
      headers: {accept: 'application/json', 'X-API-Key': 'rWezjfFa73qFAnNHCex0XmAS4FXaPFeNkX3WtwFYFmDADCRzH8d00aUtE82K7yED'}
    };
    const response = await axios.request(options);
    const listNft = response.data.result;
    return listNft;
  }
 
  return (
    <div className="App">
      <button style={{display:"block", marginBottom: "10px"}} onClick={requestAccount}>{buttonText}</button>
      <h3>Wallet address: {defaultAccount}</h3>
      <h3>Blances: {userBalance}</h3>
      {errorMessage}
      {listCollected && <NFTContainer listCollected={listCollected} />}
    </div>
  );
}

export default App;
