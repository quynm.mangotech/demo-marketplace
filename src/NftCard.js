import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import React from "react";
import { Link } from 'react-router-dom';
import * as isIPFS from 'is-ipfs'

const NFTCard = ({nft}) => {
    const name = nft.name;
    const token_id = nft.token_id;
    const metadata = nft.metadata;
    const contractAddress = nft.token_address;
    const getUrlImage = (value) => {
        if(value.includes('ipfs://')) {
          const cid = value.replace('ipfs://', '');
          if(isIPFS.cid(cid)) {
            return `https://ipfs.io/ipfs/${cid}`;
          }
          return "https://testnets.opensea.io/static/images/placeholder.png"
        }
        return value;
      }

    return (
        <Link to={`/${contractAddress}/${token_id}`}>
            <Card style={{ width: '18rem', display: "inline-block", marginRight: "2rem", marginBottom: "2rem" }}>
                <Card.Img variant="top" src={metadata && JSON.parse(metadata).image ?  getUrlImage(JSON.parse(metadata).image) : "https://testnets.opensea.io/static/images/placeholder.png"}/>
                <Card.Body>
                    <Card.Title>{metadata ? JSON.parse(metadata).name : (name + '#' + token_id).slice(0,30)}</Card.Title>
                    <Button variant="primary">Access</Button>
                </Card.Body>
            </Card>
        </Link>
    )
}
export default NFTCard;
