import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import './index.css';
import NotFound from './pages/NotFound';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
import Detail from './pages/Detail';
import Collection from './pages/Collection';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Router>
      <Routes>
        <Route path='/' element={<App />}></Route>
        <Route path=":addressContract/:tokenId"  element={<Detail />}></Route>
        <Route path="/:addressContract" element={<Collection />}></Route>
        <Route path='*' element={<NotFound />} />
      </Routes>
    </Router>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
