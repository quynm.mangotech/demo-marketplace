import axios from "axios";
import React, { useEffect, useState } from "react";
import { Container, Form, Button} from "react-bootstrap";
import { useParams } from 'react-router-dom'
import NFTContainer from "../NftContainer";

const Collection = () => {
    const [listCollection, setListCollection] = useState([]);
    const [listFilter, setListFilter] = useState([]);
    const [totalNft, setTotalNft] = useState(0);
    const [valueSearch, setValueSearch] = useState("");
    const [filter, setFilter] = useState("");

    const params = useParams();
    const addressContract = params.addressContract;
    useEffect( () => {
        async function fetchData() {
            const response = await getListNftOfCollection(addressContract)
            setListCollection(response.data.result);
            setTotalNft(response.data.total);
        }
        fetchData();
      }, [addressContract, listCollection])
    const getListNftOfCollection = async(addrContract) => {
        const options = {
            method: 'GET',
            url: `https://deep-index.moralis.io/api/v2/nft/${addrContract}`,
            params: {
                chain: 'goerli', 
                format: 'decimal',
                normalizeMetadata: 'false'
            },
            headers: {accept: 'application/json', 'X-API-Key': 'rWezjfFa73qFAnNHCex0XmAS4FXaPFeNkX3WtwFYFmDADCRzH8d00aUtE82K7yED'}
          };
        const response = await axios.request(options);
        return response;
    }

    const getListFilter = async (addrContract, valueFilter) => {
        const options = {
            method: 'GET',
            url: 'https://deep-index.moralis.io/api/v2/nft/search',
            params: {
              chain: 'goerli',
              format: 'decimal',
              q: valueFilter,
              filter: 'attributes',
              addresses: addrContract
            },
            headers: {accept: 'application/json', 'X-API-Key': 'rWezjfFa73qFAnNHCex0XmAS4FXaPFeNkX3WtwFYFmDADCRzH8d00aUtE82K7yED'}
        };
        const response = await axios.request(options);
        return response.data.result;
    }
    

    const submit = async () => {
        if(valueSearch.length === 0) {
            setFilter("");
            return;
        }
        if(valueSearch.length < 3) {
            return;
        }
        setFilter(valueSearch);
        setListFilter(await getListFilter(addressContract, valueSearch));
        
    }
    return (
        <Container style={{marginTop: "2rem"}}>
            <Form.Group style={{marginBottom: "2rem"}}>
                <Form.Control
                    as="textarea"
                    placeholder="Search by attribute"
                    value={valueSearch}
                    onChange={e => setValueSearch(e.target.value)}
                    type="text"
                />
                <Button
                  onClick={() => submit()}
                >
                    Search
                </Button>
            </Form.Group>
            <h1 style={{marginBottom: "2rem"}}>Collection: {addressContract}</h1>
            <h3>Item: {totalNft}</h3>
            <h3>Chain: Goerli</h3>
            <NFTContainer listCollected={filter ? listFilter :listCollection} />
        </Container>
    )
}

export default Collection;
