import React, { useEffect, useState } from 'react'
import { Link, useParams } from 'react-router-dom'
import axios from 'axios';
import {Container, Row, Col, Figure, Button, Toast, ToastContainer} from 'react-bootstrap';
import ActivitiesTable from '../ActivitiesTable';
import * as isIPFS from 'is-ipfs'

const Detail = () => {
  const [nftInfo, setNftInfo] = useState({});
  const [nftMetadata, setNftMetadata] = useState({});
  const [nftAttribute, setNftAttribute] = useState([])
  const [activities, setActivities] = useState([]);
  const [show, setShow] = useState(false);
  const params = useParams();
  const addressContract = params.addressContract;
  const tokenId = params.tokenId;
  useEffect(() => {
      async function fetchData() {
          const nft = await getInfo(addressContract, tokenId)
          setNftInfo(nft);
          setNftMetadata(JSON.parse(nft.metadata));
          setActivities(await getActivities(addressContract, tokenId));
          const totalNft = await getQuantityNftInCollection(addressContract)
          if(nft.metadata) {
            const attributes = JSON.parse(nft.metadata).attributes;
            for(let i=0; i< attributes.length; i++) {
              const quantity = await countRare(addressContract, attributes[i].value);
              const rare = quantity / totalNft * 100;
              attributes[i].rare = rare.toFixed();
            }
            setNftAttribute(attributes);
          }
      }
      fetchData();
    }, [addressContract, tokenId])

  const getInfo = async (addrContract, tokenId) => {
      const options = {
          method: 'GET',
          url: `https://deep-index.moralis.io/api/v2/nft/${addrContract}/${tokenId}/owners`,
          params: {chain: 'goerli', format: 'decimal', normalizeMetadata: 'false'},
          headers: {accept: 'application/json', 'X-API-Key': 'rWezjfFa73qFAnNHCex0XmAS4FXaPFeNkX3WtwFYFmDADCRzH8d00aUtE82K7yED'}
        };
      const response = await axios.request(options);
      return response.data.result[0]
  }

  const getActivities = async (addrContract, tokenId) => {
    const options = {
      method: 'GET',
      url: `https://deep-index.moralis.io/api/v2/nft/${addrContract}/${tokenId}/transfers`,
      params: {chain: 'goerli', format: 'decimal'},
      headers: {accept: 'application/json', 'X-API-Key': 'rWezjfFa73qFAnNHCex0XmAS4FXaPFeNkX3WtwFYFmDADCRzH8d00aUtE82K7yED'}
    };
    const response = await axios.request(options);
    return response.data.result;
  }

  const resyncMetadata = async (addrContract, tokenId) => {
    try{
      const options = {
        method: 'GET',
        url: `https://deep-index.moralis.io/api/v2/nft/${addrContract}/${tokenId}/metadata/resync`,
        params: {chain: 'goerli', flag: 'uri', mode: 'async'},
        headers: {accept: 'application/json', 'X-API-Key': 'rWezjfFa73qFAnNHCex0XmAS4FXaPFeNkX3WtwFYFmDADCRzH8d00aUtE82K7yED'}
      };
      await axios.request(options);
      setShow(true);
      console.log("OK")
    } catch(err){
      console.log(err)
    }
  }

  const countRare = async (addrContract, value) => {
    const options = {
      method: 'GET',
      url: 'https://deep-index.moralis.io/api/v2/nft/search',
      params: {
        chain: 'goerli', 
        format: 'decimal',
        q: value,
        filter: 'attributes',
        addresses: addrContract,
        limit: '1'
      },
      headers: {accept: 'application/json', 'X-API-Key': 'rWezjfFa73qFAnNHCex0XmAS4FXaPFeNkX3WtwFYFmDADCRzH8d00aUtE82K7yED'}
    };
    const response = await axios.request(options);
    return response.data.total;
  }

  const getQuantityNftInCollection = async(addrContract) => {
    const options = {
        method: 'GET',
        url: `https://deep-index.moralis.io/api/v2/nft/${addrContract}`,
        params: {
            chain: 'goerli', 
            format: 'decimal',
            normalizeMetadata: 'false'
        },
        headers: {accept: 'application/json', 'X-API-Key': 'rWezjfFa73qFAnNHCex0XmAS4FXaPFeNkX3WtwFYFmDADCRzH8d00aUtE82K7yED'}
      };
    const response = await axios.request(options);
    return response.data.total;
  }
  const getUrlImage = (value) => {
    if(value.includes('ipfs://')) {
      const cid = value.replace('ipfs://', '');
      if(isIPFS.cid(cid)) {
        return `https://ipfs.io/ipfs/${cid}`;
      }
      return "https://testnets.opensea.io/static/images/placeholder.png"
    }
    return value;
  }
  // nftMetadata && getUrlImage(nftMetadata.image)

  return (
    <Container style={{marginTop: "2rem"}}>
      <Row>
        <Col>
          <Figure>
            <Figure.Image
              width={600}
              height={360}
              alt="550x360"
              src={nftMetadata && nftMetadata.image ? getUrlImage(nftMetadata.image) : "https://testnets.opensea.io/static/images/placeholder.png"}
            />
          </Figure>
        </Col>
        <Col>
          <h1>{nftMetadata && nftMetadata.name ? nftMetadata.name : (nftInfo.name + '#' + nftInfo.token_id).slice(0,30)}</h1>
          <div>
            <h4 style={{marginTop: "2rem"}}>Owned by: </h4>
            <h6>{nftInfo.owner_of}</h6>
          </div>
          <div>
            <h4 style={{marginTop: "2rem"}}>Details: </h4>
            <h6>Contract Address: 
              <Link to={`../../${nftInfo.token_address}`}> {nftInfo.token_address}</Link>
            </h6>
            <h6>Token ID: {nftInfo.token_id}</h6>
            <h6>Token Standard: {nftInfo.contract_type}</h6>
            <h6>Chain: Goerli</h6>
          </div>
          <div style={{marginBottom: "2rem"}}>
            <h4 style={{marginTop: "2rem"}}>Properties: </h4> 
            {nftAttribute && nftAttribute.map((attribute, index) => {
              return <h6 key={index}>{attribute.trait_type}: {attribute.value} ====={'>'} {Number(attribute.rare)}% have this trait</h6>
            })}
          </div>
          <Button variant="primary" onClick={() => resyncMetadata(addressContract,tokenId)}>Refesh Metadata</Button>
        </Col>
      </Row>
      <div>
        <h4 style={{marginTop: "2rem"}}>Description: </h4>
        <p>{nftMetadata && nftMetadata.description}</p>
      </div>
      <div>
        <h4 style={{marginTop: "2rem"}}>Activities: </h4>
        <ActivitiesTable activities={activities}/>
      </div>
      <ToastContainer position="top-end">
        <Row>
          <Col>
            <Toast bg="success" onClose={() => setShow(false)} show={show} delay={3000} autohide>
              <Toast.Header>
                <img
                  src="holder.js/20x20?text=%20"
                  className="rounded me-2"
                  alt=""
                />
                <strong className="me-auto">Notifiaction</strong>
                <small>Now</small>
              </Toast.Header>
              <Toast.Body>Refesh metadata successfully!</Toast.Body>
            </Toast>
          </Col>
        </Row>
      </ToastContainer>
    </Container>
  )
}

export default Detail;