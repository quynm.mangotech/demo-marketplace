import React from "react";
import {Table} from 'react-bootstrap';

const ActivitiesTable = ({activities}) => {
    return (
        <Table >
            <thead>
            <tr>
                <th>Event</th>
                <th>Value</th>
                <th>From</th>
                <th>To</th>
                <th>Scan</th>
            </tr>
            </thead>
            <tbody>
            {activities && activities.map((activity, index) => {
                return <tr key={index}>
                <td>{activity.from_address === "0x0000000000000000000000000000000000000000" ? "Minted" : "Transfer"}</td>
                <td>{activity.value/10**18} ETH</td>
                <td>{activity.from_address}</td>
                <td>{activity.to_address}</td>
                <td>
                    <a href={`https://goerli.etherscan.io/tx/${activity.transaction_hash}`}>More</a>
                </td>
            </tr>;
            })}
            </tbody>
        </Table>
    )
}
export default ActivitiesTable;
